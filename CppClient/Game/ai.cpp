#include "ai.h"

#include <ctime>
#include <vector>
#include <iostream>
#include <map>
#include <cmath>

#define __DEBUG__ 1

using namespace std;
using namespace koala::chillin::client;
using namespace ks::models;
using namespace ks::commands;

Position lastPos;
vector<Position> lastPoses;
/*
class EWall: public Wall
{
    EWall(Wall wall)
    {
        this->__end_pos = wall.__end_pos;
        this->__start_pos = wall.__start_pos;
    }

    void extend(float len)
    {

    }
};
*/
bool comPair(const pair<int, int>& _a, const pair<int, int>& _b)
{
    return _a.first < _b.first;
}

bool operator==(const Position& _a, const Position& _b)
{
    float dx = (_a.x() - _b.x());
    float dy = (_a.y() - _b.y());
    return (abs(dx) < 0.0001) && (abs(dy) < 0.0001);
}

inline bool inVector(const int &e, const vector<int> &v)
{
    return (find(v.begin(), v.end(), e) != v.end());
}

inline float _dist(const Position& _a, const Position& _b)
{
    float dx = (_a.x()) - (_b.x());
    float dy = (_a.y()) - (_b.y());
    return sqrt(dx*dx + dy*dy);
}

struct MoveCost
{
    float displacement;
    float turn;
};

class mVector
{
    float x;
    float y;
    float _slope;
    float _tg;

    Position head;
    Position tail;

public:
    const float PI = 3.1415;
    float magnitude;

    mVector(float _slope, int magnitude = 1)
    {
        float theta = _slope * PI/180;
        this->_slope = _slope;
        this->magnitude = magnitude;
        this->x = cos(theta) * this->magnitude;
        this->y = sin(theta) * this->magnitude;
    }

    mVector(float x, float y)
    {
        this->x = x;
        this->y = y;
        this->magnitude = sqrt(x*x + y*y);
        this->setSlope();
    }
    mVector(Position start, Position end)
    {
        this->head = start;
        this->tail = end;
        this->x = end.x() - start.x();
        this->y = end.y() - start.y();
        this->magnitude = sqrt(x*x + y*y);
        this->setSlope();
    }

    inline void setSlope()
    {
        if(this->x == 0)
        {
            this->_slope = 90;
        }
        else
        {
            this->_slope = atan(this->y / this->x) * 180/PI;
        }

        if(this->x < 0 && this->_slope < 0)
        {
            this->_slope += 180;
        }
        if(this->x > 0 && this->_slope > 0)
        {
            this->_slope -= 180;
        }

        //this->_slope = round(_slope);
    }

    float y_intercept() const
    {
        if(slope() == 90)
        {
            return -1;
        }
        return this->head.y() - tan(this->slope()*PI/180)*this->head.x();
    }

    float x_of_intersection(const mVector &v)
    {
        if(v.slope() == 90)
        {
            return v.head.x();
        }
        if(this->slope() == 90)
        {
            return this->head.x();
        }
        float mV = tan(v.slope() * PI/180);
        float mT = tan(this->slope() * PI/180);

        return (mV*v.head.x()-mT*this->head.x()) / (mT-mV);
    }

    bool intersects(const mVector &v)
    {
        //We assume that vector's head and/or tail are/is assigned properly
        //equation for *this:   y-yt = this->slope * (x-xt)
        //equation for v:       y-yv =     v.slope * (x-xv)
        //intersection point: (x: [mvxv-mtxt]/[mt-mv], y: yv + mv(x-xv))

        if(v || *this)
        {
            float v_y_intercept = v.y_intercept();
            float this_y_intercept = this->y_intercept();
            if(abs(v_y_intercept - this_y_intercept) > 0.001)
            {
                return false;
            }
            return (v.belongsTo(this->head.x()) ||
                    v.belongsTo(this->tail.x()) ||
                    this->belongsTo(v.head.x()) ||
                    this->belongsTo(v.tail.x()) );
        }
        else
        {
            float x_i = this->x_of_intersection(v);
            //float y_i = v.head.y()+v.slope()*(x_i-v.head.x());
            if(this->belongsTo(x_i) && v.belongsTo(x_i))
            {
                return true;
            }
            return false;
        }
    }

    inline bool belongsTo(float x) const
    {
        return (min(this->head.x(), this->tail.x()) - 0.001 < x &&
                max(this->head.x(), this->tail.x()) + 0.001 > x);
    }

    float slope() const
    {
        return this->_slope;
    }

    bool operator||(const mVector &vec) const
    {
        float tmp = (this->slope() - vec.slope());
        return tmp < 0.001 && tmp > -0.001;
    }

    float operator*(const mVector &vec)
    {
        return (vec.x * this->x + vec.y * this->y);
    }

    float operator^(const mVector &vec)
    {
        float theta = 0;
        theta = vec._slope - this->_slope;
        if(theta > 180)
        {
            theta -= 360;
        }
        else if(theta < -180)
        {
            theta += 360;
        }

        return theta;
    }
};

inline bool isWallInWay(const Wall &wall, const Position &first, const Position &second)
{
    mVector vWall(wall.start_pos(), wall.end_pos());
    mVector vPath(first, second);

    return (vWall.intersects(vPath));
}

bool AI::hasWallInWay(Position first, Position second)
{
    for(int i = 0; i < this->world->walls().size(); i++)
    {
        if(isWallInWay(this->world->walls()[i], first, second))
        {
            return true;
        }
    }
    return false;
}

AI::AI(World *world): RealtimeAI<World*>(world)
{
    srand(time(0));
}

void AI::initialize()
{
    this->extendWalls();
    this->updateEnemyHistory();
    this->updateSelfHistory();
    this->reassignMedics();
}

/*
int AI::distAid(const Position &start, const Position &end, const Medic &passer)
{
    ///TODO:
    /// radius of player is not considered yet
    /// not tested

    if(start == end)
    {
        return 0;
    }

    if(!hasWallInWay(start, end))
    {
        int result = ceil(_dist(start, end)/passer.max_move_distance());
        return result;
    }

    vector< pair<int, int> > costsOfWall;
                             //cost of walls stored as a pair of cost wallID
                             //wallID = (wallIndex * 2) + (end ? 1 : 0)
    Position temp;
    for(int i = 0; i < this->world->walls().size(); i++)
    {
        temp = this->world->walls()[i].start_pos();
        if(!inVector(i<<1, this->visitedWallIDs) && !(start == temp) && !hasWallInWay(start, temp))
        {
            this->visitedWallIDs.push_back(i<<1);
            costsOfWall.push_back(make_pair(distAid(temp, end, passer), i<<1));
        }

        temp = this->world->walls()[i].end_pos();
        if(!inVector((i<<1)+1, this->visitedWallIDs) && !(start == temp) && !hasWallInWay(start, temp))
        {
            this->visitedWallIDs.push_back((i<<1)+1);
            costsOfWall.push_back(make_pair(distAid(temp, end, passer), (i<<1)+1));
        }
    }

    if(costsOfWall.empty())
    {
        return 100000;
    }

    pair<int, int> minCost = *min_element(costsOfWall.begin(), costsOfWall.end(), comPair);
    temp = (minCost.second & 1) ?
                this->world->walls()[minCost.second>>1].end_pos() :
                this->world->walls()[minCost.second>>1].start_pos();
    int directDist = ceil(_dist(start, temp)/passer.max_move_distance());
    mVector destination(passer.position(), temp);
    mVector player(passer.angle(), 1);
    float angleNeeded = destination ^ player;
    float angleCost = ceil(abs(angleNeeded)/passer.max_turn_angle());
    //if(angleCost > 0) angleCost--;
    lastPos = temp;
    int result = angleCost + directDist + minCost.first;
    return result;
}
*/

int estimateCost(const Position &start, const Position &end, const Medic &passer)
{
    int result = 0;
    mVector path(start, end);
    mVector player(passer.angle());
    float angleNeeded = player^path;
    int angleCost = ceil(abs(angleNeeded)/passer.max_turn_angle());
    int distCost = ceil(_dist(start, end)/passer.max_turn_angle());
    result = angleCost + distCost;

    return result;
}

int AI::distAid(const Position &start, const Position &end, const Medic &passer)
{
    if(start == end)
    {
        lastPos = end;
        return 0;
    }
    if(!this->hasWallInWay(start, end))
    {
        lastPos = end;
        return estimateCost(start, end, passer);
    }
    Position temp;
    vector< pair<int, int> > costs;         //pairs of cost and id

    for(unsigned i = 0; i < this->world->walls().size(); i++)
    {
        temp = this->world->walls()[i].start_pos();
        if(!(temp == start) && !hasWallInWay(temp, start) && !inVector(i<<1, this->visitedWallIDs))
        {
            this->visitedWallIDs.push_back(i<<1);
            int estimatedCost = estimateCost(start, temp, passer) + distAid(temp, end, passer);
            costs.push_back(make_pair(estimatedCost, i<<1));
        }
        temp = this->world->walls()[i].end_pos();
        if(!(temp == start) && !hasWallInWay(temp, start) && !inVector((i<<1)+1, this->visitedWallIDs))
        {
            this->visitedWallIDs.push_back((i<<1)+1);
            int estimatedCost = estimateCost(start, temp, passer) + distAid(temp, end, passer);
            costs.push_back(make_pair(estimatedCost, (i<<1)+1));
        }
    }

    if(costs.empty())
    {
        return 100000;
    }
    pair<int, int> minCost = *min_element(costs.begin(), costs.end(), comPair);
    lastPos = (minCost.second & 1 ? (this->world->walls()[minCost.second>>1].end_pos()) : (this->world->walls()[minCost.second>>1].start_pos()));

    return minCost.first;
}

int AI::dist(const Position &start, const Position &end, const Medic &passer)
{
    this->visitedWallIDs.clear();
    int result = this->distAid(start, end, passer);
    this->visitedWallIDs.clear();
#ifdef __DEBUG__
    cout << "Dist for (" << start.x() << ", " << start.y() << ") and (" << end.x() << ", " << end.y() << ") is:\n";
#endif
#ifdef __DEBUG__
    cout << result << "\n";
#endif
    return result;
}

void AI::myStrategy()
{
    if(this->isReassignNeeded())
    {
        this->reassignMedics();
    }
    for(int i = 0; i < this->world->ref_medics()[this->mySide].size(); i++)
    {
        Medic medic = this->world->ref_medics()[this->mySide][i];
        if(this->getRole(medic) == HEALER)
        {
            this->doHealerJob(this->world->ref_medics()[this->mySide][i]);
        }
        else if(this->getRole(medic) == KILLER)
        {
            this->doKillerJob(this->world->ref_medics()[this->mySide][i]);
        }
    }
}

void AI::antiEnemyStrategy()
{
    ///TODO
}

void AI::reassignMedics()
{
    this->roles.clear();
    int myLives = this->world->ref_medics()[this->mySide].size();
    int otherLives = this->world->ref_medics()[this->otherSide].size();
    if(myLives <= otherLives)
    {
        for(int i=0;i<this->world->ref_medics()[this->mySide].size();i++)
        {
            this->roles.insert(&this->world->ref_medics()[this->mySide][i],HEALER);
        }
    }
    else
    {
        int bestKillerIndex = 0;
        int maxLaser = -1;
        for(int i=0;i<this->world->ref_medics()[this->mySide].size();i++)
        {
            if(this->world->ref_medics()[this->mySide][i].laser_count() > maxLaser)
            {
                maxLaser = this->world->ref_medics()[this->mySide][i].laser_count();
                bestKillerIndex = i;
            }
        }
        for(int i=0;i<this->world->ref_medics()[this->mySide].size();i++)
        {
            if(i==bestKillerIndex)
            {
                this->roles.insert(&this->world->ref_medics()[this->mySide][i],KILLER);
            }
            else
            {
                this->roles.insert(&this->world->ref_medics()[this->mySide][i],HEALER);
            }
        }
    }

}

void AI::doKillerJob(const Medic &medic)
{
    for(int i=0;i<this->world->ref_medics()[this->otherSide].size();i++)
    {
        if(this->inKillArea(medic,this->world->ref_medics()[this->otherSide][i]))
        {
            this->fireToEnemy(medic,this->world->ref_medics()[this->otherSide][i]);
            return;
        }
    }
    for(int i=0;i<this->world->ref_patients().size();i++)
    {
        if(this->inHealArea(medic,this->world->ref_patients()[i]))
        {
            return;
        }
    }
    if(this->doNeedPowerUp(medic))
    {
        this->goToPosition(medic,this->bestPowerUp(medic)->position());
        return;
    }
    else {
        this->goToPosition(medic,this->bestMedic(medic)->position());
    }

}
void AI::doHealerJob(const Medic &medic)
{
    for(int i=0;i<this->world->ref_patients().size();i++)
    {
        if(this->inHealArea(medic,this->world->ref_patients()[i]))
        {
            return;
        }
    }
    for(int i=0;i<this->world->ref_medics()[this->otherSide].size();i++)
    {
        if(this->inKillArea(medic,this->world->ref_medics()[this->otherSide][i]))
        {
            this->fireToEnemy(medic,this->world->ref_medics()[this->otherSide][i]);
            return;
        }
    }
    if(this->doNeedPowerUp(medic))
    {
        this->goToPosition(medic,this->bestPowerUp(medic)->position());
        return;
    }
    else
    {
        this->goToPosition(medic,this->bestPatient(medic)->position());
        return;
    }

}

AI::Role AI::getRole(const Medic &medic)
{
    return this->roles[&medic];
}
Position AI::nextStep(const Medic& medic)
{
    ///TODO
}
void AI::goToPosition(const Medic& medic,const Position &pos)
{
    ///TODO
    //dist(medic.position(), pos, medic);
    mVector destination(medic.position(), lastPos);
    mVector player(medic.angle(), 1);
    float angleNeeded = player ^ destination;
    bool clockwise = angleNeeded < 0;
    if(angleNeeded < 0) angleNeeded *= -1;
    float directDist = _dist(medic.position(), pos);
#ifdef __DEBUG__
    cout << "Medic" << medic.id() << ": (" << medic.position().x() << ", " << medic.position().y() << ") -> (" << pos.x() << ", " << pos.y() << ")\n";
    //cout << "Medic" << medic.id() << ": (" << medic.angle() << ") -> (" << angleNeeded << ")\n";
#endif
    if(angleNeeded > medic.max_turn_angle())
    {
        turn(medic.id(), clockwise, medic.max_turn_angle());
    }
    else if(angleNeeded > 5)
    {
        turn(medic.id(), clockwise, angleNeeded);
    }
    else if(directDist > medic.max_move_distance())
    {
        move(medic.id(), medic.max_move_distance());
    }
    else
    {
        move(medic.id(), directDist);
    }
}

vector<Patient *> AI::nearestPatients(const Medic &medic)
{
    lastPoses.clear();
    vector<Patient *> result;
    vector<int> distances;
    int tmp;
    Patient* temp;
    Position Temp;

    for(int i=0;i<this->world->ref_patients().size();i++)
    {
        result.push_back(&this->world->ref_patients()[i]);
    }
    for(int i=0;i<this->world->ref_patients().size();i++)
    {
        distances.push_back(this->dist(medic.position(),this->world->ref_patients()[i].position(),medic));
        lastPoses.push_back(lastPos);
    }
    for (int i = 0; i < distances.size(); i++)
    {
       for (int j = i+1; j < distances.size(); j++)
       {
            if (distances[i] > distances[j])
            {
              tmp = distances[i];
              distances[i] = distances[j];
              distances[j] = tmp;
              temp = result[i];
              result[i] = result[j];
              result[j] = temp;
              Temp = lastPoses[i];
              lastPoses[i] = lastPoses[j];
              lastPoses[j] = Temp;
            }
       }
    }
    return result;
}

vector<Medic*> AI::nearestMedics(const Medic &medic)
{
    lastPoses.clear();
    string medicSide = medic.side_name();
    string otherMedicsSide = (medicSide == this->mySide) ? this->otherSide : this->mySide ;
    vector<Medic*> result;
    vector<int> distances;
    int tmp;
    Medic* temp;
    Position Temp;

    for(int i=0;i<this->world->ref_medics()[otherMedicsSide].size();i++)
    {
        result.push_back(&this->world->ref_medics()[otherMedicsSide][i]);
    }
    for(int i=0;i<this->world->ref_medics()[otherMedicsSide].size();i++)
    {
        distances.push_back(this->dist(medic.position(),this->world->ref_medics()[otherMedicsSide][i].position(),medic));
        lastPoses.push_back(lastPos);
    }
    for (int i = 0; i < distances.size(); i++)
    {
       for (int j = i+1; j < distances.size(); j++)
       {
            if (distances[i] > distances[j])
            {
              tmp = distances[i];
              distances[i] = distances[j];
              distances[j] = tmp;
              temp = result[i];
              result[i] = result[j];
              result[j] = temp;
              Temp = lastPoses[i];
              lastPoses[i] = lastPoses[j];
              lastPoses[j] = Temp;
            }
       }
    }
    return result;
}

vector<PowerUp *> AI::nearestPowerUps(const Medic &medic)
{
    lastPoses.clear();
    vector<PowerUp *> result;
    vector<int> distances;
    int tmp;
    PowerUp* temp;
    Position Temp;
    for(int i=0;i<this->world->ref_powerups().size();i++)
    {
        result.push_back(&this->world->ref_powerups()[i]);
    }
    for(int i=0;i<this->world->ref_powerups().size();i++)
    {
        distances.push_back(this->dist(medic.position(),this->world->ref_powerups()[i].position(),medic));
        lastPoses.push_back(lastPos);
    }
    for (int i = 0; i < distances.size(); i++)
    {
       for (int j = i+1; j < distances.size(); j++)
       {
            if (distances[i] > distances[j])
            {
              tmp = distances[i];
              distances[i] = distances[j];
              distances[j] = tmp;
              temp = result[i];
              result[i] = result[j];
              result[j] = temp;
              Temp = lastPoses[i];
              lastPoses[i] = lastPoses[j];
              lastPoses[j] = Temp;
            }
       }
    }
    return result;
}

Patient *AI::bestPatient(const Medic &medic)
{
    vector<Patient*> nears = this->nearestPatients(medic);
    if(this->getRole(medic) == HEALER)
    {
        if(this->world->ref_medics()[this->mySide].size() == this->world->ref_medics()[this->otherSide].size())
        {
            for(int i=0;i<nears.size();i++)
            {
                if(nears[i]->capturable())
                {
                    lastPos = lastPoses[i];
                    return nears[i];
                }
            }
            lastPos = lastPoses[0];
            return nears[0];
        }
    }
    else
    {
        lastPos = lastPoses[0];
        return nears[0];
    }
}

Medic *AI::bestMedic(const Medic &medic)
{
    vector<Medic*> nears = this->nearestMedics(medic);
    int minFactor = medic.max_health() + 10;
    int bestMedicIndex = 0;
    for(int i=0;i<nears.size();i++)
    {
        if(nears[i]->health() + 2*i < minFactor)
        {
            minFactor = nears[i]->health() + 2*i;
            bestMedicIndex = i;
        }
    }
    lastPos = lastPoses[bestMedicIndex];
    return nears[bestMedicIndex];
}

PowerUp *AI::bestPowerUp(const Medic &medic)
{
    vector<PowerUp *> powerUps = this->nearestPowerUps(medic);
    if (this->getRole(medic)==HEALER)
    {
        if(medic.health() < medic.max_health()/2)
        {
            for(int i=0;i<powerUps.size();i++)
            {
                if(powerUps[i]->type() == PowerUpType::HEAL_PACK)
                {
                    lastPos = lastPoses[i];
                    return powerUps[i];
                }
            }
        }
        else
        {
            return nullptr;
        }
    }
    if (this->getRole(medic)==KILLER)
    {
        if(medic.laser_count() < medic.laser_max_count()/2)
        {
            for(int i=0;i<powerUps.size();i++)
            {
                if(powerUps[i]->type() == PowerUpType::LASER)
                {
                    lastPos = lastPoses[i];
                    return powerUps[i];
                }
            }
        }
        else
        {
            return nullptr;
        }
    }
}

vector<Medic *> AI::weakestMedic(const Medic &medic)
{
    lastPoses.clear();
    string medicSide = medic.side_name();
    string otherMedicsSide = (medicSide == this->mySide) ? this->otherSide : this->mySide ;
    vector<Medic *> result;
    vector<int> healths;
    int tmp;
    Medic* temp;
    Position Temp;
    for(int i=0;i<this->world->ref_medics()[otherMedicsSide].size();i++)
    {
        result.push_back(&this->world->ref_medics()[otherMedicsSide][i]);
    }
    for(int i=0;i<this->world->ref_medics()[otherMedicsSide].size();i++)
    {
        healths.push_back(this->world->ref_medics()[otherMedicsSide][i].health());
        lastPoses.push_back(lastPos);
    }
    for (int i = 0; i < healths.size(); i++)
    {
       for (int j = i+1; j < healths.size(); j++)
       {
            if (healths[i] > healths[j])
            {
              tmp = healths[i];
              healths[i] = healths[j];
              healths[j] = tmp;
              temp = result[i];
              result[i] = result[j];
              result[j] = temp;
              Temp = lastPoses[i];
              lastPoses[i] = lastPoses[j];
              lastPoses[j] = Temp;
            }
       }
    }
    return result;

}

bool AI::inHealArea(const Medic &medic, const Patient &patient)
{
    float diff = _dist(medic.position(),patient.position());
    if(diff<medic.radius()+patient.radius())
    {
        return true;
    }
    return false;
}

bool AI::inKillArea(const Medic &medic, const Medic &enemy)
{
    float diff = _dist(medic.position(),enemy.position());
    float theta =  abs(medic.angle()-enemy.angle());
    if(diff < medic.laser_range() + enemy.radius() && theta <= medic.max_fire_angle() )
    {
        return true;
    }
    return false;
}

bool AI::doNeedPowerUp(const Medic &medic)
{
    if(this->getRole(medic) == HEALER)
    {
        if(medic.health()<medic.max_health()/2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        if(medic.laser_count()<medic.laser_max_count()/2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

void AI::fireToEnemy(const Medic &medic, const Medic &enemy)
{
    ///TODO
}

bool AI::isReassignNeeded()
{
    return (isLivesChanged() || isBulletsChanged()) ;
}
bool AI::isBulletsChanged()
{
    ///we Think number of lives has not changed in this method
    if(lastSelfState.size()!=this->world->ref_medics()[this->mySide].size())
    {
        return false;
    }
    for(int i=0;i<this->world->ref_medics()[this->mySide].size();i++)
    {
        Medic medic = this->world->ref_medics()[this->mySide][i];
        if(medic.laser_count() == 0 && lastSelfState[i].laser_count()!=0)
        {
            return true;
        }
        if(medic.laser_count() == medic.laser_max_count() && lastSelfState[i].laser_count()!=medic.laser_max_count())
        {
            return true;
        }
    }
    return false;
}

void AI::updateSelfHistory()
{
    lastSelfState.clear();
    for(int i=0;i<this->world->ref_medics()[this->mySide].size();i++)
    {
        lastSelfState.push_back(this->world->ref_medics()[this->mySide][i]);
    }
}

void AI::updateEnemyHistory()
{
    lastEnemyState.clear();
    for(int i=0;i<this->world->ref_medics()[this->otherSide].size();i++)
    {
        lastEnemyState.push_back(this->world->ref_medics()[this->otherSide][i]);
    }
}

Decision AI::changesToEnemy(Medic *medic)
{
    ///TODO
}
bool AI::isLivesChanged()
{
    int livesInPrevCycle = lastEnemyState.size() + lastSelfState.size();
    int livesInThisCycle = world->ref_medics()[this->mySide].size() + world->ref_medics()[this->otherSide].size();
    return (livesInPrevCycle != livesInThisCycle);
}

void AI::decide()
{
    this->myStrategy();
    this->updateSelfHistory();
    this->updateEnemyHistory();
    /*for (int i = 0; i < this->world->ref_medics()[this->mySide].size(); i++)
    {
        Medic medic = this->world->ref_medics()[this->mySide][i];
        int rand = getRandInt(0, 6);

        if (rand == 0)
        {
            this->move(
                medic.id(),
                getRandFloat(0, medic.max_move_distance())
            );
        }
        else if (rand == 1)
        {
            this->turn(
                medic.id(),
                (bool) getRandInt(0, 1),
                getRandFloat(0, medic.max_turn_angle())
            );
        }
        else if(rand == 2)
        {
            this->fire(
                medic.id(),
                (bool) getRandInt(0, 1),
                getRandFloat(0, medic.max_fire_angle())
            );
        }
    }*/
}

int AI::getRandInt(int start, int end)
{
    return (rand() % (end - start + 1)) + start;
}

float AI::getRandFloat(float start, float end)
{
    float n = ((float) rand()) / ((float) RAND_MAX);
    return n * (end - start) + start;
}

void AI::extendWalls()
{
    for(int i = 0; i < this->world->walls().size(); i++)
    {
        this->extendWall(this->world->walls()[i]);
    }
}

void AI::extendWall(const Wall &wall)
{
    //EWall extended = wall;
    //extended.__end_pos = Position();
}


void AI::move(int medicId, float distance)
{
#ifdef __DEBUG__
    cout << "Medic" << medicId << " moves " << distance << "\n";
#endif
    Move cmd;
    cmd.id(medicId);
    cmd.distance(distance);
    this->sendCommand(&cmd);
}

void AI::turn(int medicId, bool clockwise, float angle)
{
#ifdef __DEBUG__
    cout << "Medic" << medicId << " turns " << (clockwise?'-':'+') << angle << "\n";
#endif
    Turn cmd;
    cmd.id(medicId);
    cmd.clockwise(clockwise);
    cmd.angle(angle);
    this->sendCommand(&cmd);
}

void AI::fire(int medicId, bool clockwise, float angle)
{
#ifdef __DEBUG__
    cout << "Medic" << medicId << " fires " << (clockwise?'-':'+') << angle << "\n";
#endif
    Fire cmd;
    cmd.id(medicId);
    cmd.clockwise(clockwise);
    cmd.angle(angle);
    this->sendCommand(&cmd);
}
