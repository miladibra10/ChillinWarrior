#ifndef AI_H
#define AI_H

#include <ChillinClient>
#include <vector>
#include <map>
#include "ks/models.h"
#include "ks/commands.h"
using namespace ks::models;
using namespace koala::chillin::client;
using namespace std;
using namespace ks::commands;

class Decision
{
public:
    Decision() {}
    typedef enum {TURN, FIRE, MOVE, WAIT} DecicionType;

    //turn
    float turnAngle;

    //fire
    Medic *fireTarget;

    //move
    Position from;
    Position to;

    //wait
    Patient *healTarget;

};
class AI : public koala::chillin::client::RealtimeAI<ks::models::World*>
{
private:
    int getRandInt(int start, int end);
    float getRandFloat(float start, float end);

    vector<int> visitedWallIDs;
    vector<Wall> extendedWalls;

    void extendWalls();
    void extendWall(const Wall& wall);

public:
    typedef enum {HEALER,KILLER} Role;

    AI(ks::models::World *world);
    void initialize();
    void decide();
    bool hasWallInWay(Position first, Position second);
    int dist(const Position &start, const Position &end, const Medic &passer);
    int distAid(const Position &start, const Position &end, const Medic &passer);
    void myStrategy();
    void antiEnemyStrategy();
    void reassignMedics();
    void doKillerJob(const Medic &medic);
    void doHealerJob(const Medic &medic);
    Role getRole(const Medic& medic);
    Position nextStep(const Medic& medic);
    void goToPosition(const Medic &medic, const Position& pos);
    vector<Patient*> nearestPatients(const Medic& medic);
    vector<Medic*> nearestMedics(const Medic& medic);
    vector<PowerUp*> nearestPowerUps(const Medic& medic);
    Patient* bestPatient(const Medic& medic);
    Medic* bestMedic(const Medic& medic);                   //must be implemented generic
    PowerUp* bestPowerUp(const Medic& medic);
    vector<Medic*> weakestMedic(const Medic& medic);        //must be implemented generic
    bool inHealArea(const Medic& medic,const Patient& patient);
    bool inKillArea(const Medic& medic,const Medic& enemy);
    bool doNeedPowerUp(const Medic& medic);
    void fireToEnemy(const Medic& medic,const Medic& enemy);
    bool isReassignNeeded();
    bool isLivesChanged();
    bool isBulletsChanged();
    void updateSelfHistory();
    void updateEnemyHistory();
    Decision changesToEnemy(Medic *medic);
    vector<Medic> lastEnemyState;
    vector<Medic> lastSelfState;
    vector<Position> mainNodes;
    //map<const Medic*,Role> roles;
    QHash<const Medic*,Role> roles;

protected:
    void move(int medicId, float distance);
    void turn(int medicId, bool clockwise, float angle);
    void fire(int medicId, bool clockwise, float angle);
};

#endif // AI_H
